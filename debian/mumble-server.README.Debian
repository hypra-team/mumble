This package includes a system-wide installation of murmurd. If you want a
personal server running on your own user account, use the murmur-user-wrapper
script.

Murmur's root user is called SuperUser. To set the password for superuser,
simply dpkg-reconfigure mumble-server.

The mumble-server init script is silent concering messages to the console on
startup/shutdown by default; if you wish to see start/stop messages to the
console on upgrades, uncomment "VERBOSE=yes" in /etc/default/rcS.

To enable or disable the mumble-server daemon under sysvinit, this now needs
to be done via update-rc.d at the command line as root or via sudo:

  update-rc.d mumble-server [disable|enable]

This has been changed compared to upstream because disabling daemons via an
environment variable in /etc/default/<package> causes problems for init
systems (e.g. systemd) so it has been deprecated by Debian Policy §9.3.3.1
since Policy version 4.1.3.

To stop and disable mumble-server with systemd:

  systemctl stop    mumble-server
  systemctl disable mumble-server
  systemctl mask    mumble-server

To re-enable and start mumble-server with systemd:

  systemctl unmask  mumble-server
  systemctl enable  mumble-server
  systemctl start   mumble-server
